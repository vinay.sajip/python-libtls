.. title:: python-libtls - TLS library for Python

.. python-libtls documentation master file, created by
   sphinx-quickstart on Tue Jan 31 20:22:02 2017.

What is |pname|?
================

|pncode| is a Python library which provides a high-level interface for
secure network communication using the latest versions of Transport Layer
Security (TLS). The underlying TLS functionality is provided by |libtls|,
which is part of `LibreSSL <https://www.libressl.org/>`_. |libtls| has a
simple API and good security defaults.

LibreSSL is a version of the TLS/crypto stack forked by the OpenBSD team from
OpenSSL in 2014, with the goals of modernizing the codebase, improving security,
and applying best practice development processes.  LibreSSL has been, and
continues to be, `less susceptible
<https://en.wikipedia.org/wiki/LibreSSL#Security_and_vulnerabilities>`_
to vulnerabilities than OpenSSL.

Here is a simple example of using |pncode| to securely connect to a
server, send data to it and receive data from it.

.. code-block:: pycon

  >>> from tls import Context; context = Context()
  >>> tls_socket = context.connect('github.com', 443)
  >>> tls_socket.sendall(b'GET / HTTP/1.1\r\nhost: github.com\r\n\r\n')
  >>> data = tls_socket.recv(4096)
  >>> len(data)
  1370
  >>> data[:68]
  b'HTTP/1.1 200 OK\r\nServer: GitHub.com\r\nDate: Fri, 10 Feb 2017 16:53:39'

The data sent and received over the socket will have been protected by TLS.

Usage requirements
==================

This package is compatible with Python 2.7 and Python >= 3.4. It tracks recent releases of |libtls| (the latest is |libtls_version|). It is a
pure-Python package with no other Python dependencies.

.. note::

  You *will* need the |libtls| binaries for your platform. Even if your
  operating system uses LibreSSL and hence |libtls|, the system version of
  |libtls| may not be compatible with a particular version of |pncode|.

  This project does not provide binaries, as you would have no way of verifying
  that those binaries are actually what they purport to be. You may want to
  refer to the section on :ref:`own-binaries`.

Installation and usage
======================

You can install this package using ``pip install python-libtls``. Once installed, you can access its functionality via the ``tls`` package. See the :ref:`tutorial` for more information.

Testing the installation
------------------------

If you download the source distribution, a test script ``test_tls.py`` is
provided. You can test your installation of |pncode| by running something like

.. code-block:: shell

  LD_LIBRARY_PATH=/path/to/libtls/lib python test_tls.py

License
=======

.. include:: ../LICENSE.txt

Limitations
===========

There are some specific |libtls| APIs which are not yet supported:

``tls_load_file``
  This API allocates memory and loads the contents of a file into it, but does
  not provide a corresponding ``tls_free_file``. Without it, there is no
  guaranteed safe, cross-platform way of releasing the allocated memory.

``tls_config_set_XXXX_mem``
  These may be implemented in future versions of |pncode|.

``tls_config_add_ticket_key``
  This may be implemented in future versions of |pncode|.

``tls_connect_fds``, ``tls_connect_cbs``, ``tls_accept_fds``, ``tls_accept_cbs``
  These may be implemented in future versions of |pncode|.

.. cssclass:: hidden

User Guide
==========

.. toctree::
  :maxdepth: 5

  tutorial
  reference




