$(document).ready(function() {
  var $sidebar = $('.sphinxsidebar');
  var $link = $('.sphinxsidebar > a');
  var $div = $('<div>').append($link);

  $div.prependTo($sidebar);
  $('.related').remove();
  $('.sidebar-block').eq(0).addClass('the-toc');
  $('.sidebar-block').eq(1).addClass('the-search');
  $('#right-column .breadcrumb li a').eq(0).text('Home');
});
