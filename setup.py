from distutils.core import setup

__version__ = '0.1.0'


setup(name='python-libtls',
      description='A Python binding for libtls',
      long_description=('This module allows easy access to libtls functionality '
                        'from Python programs. It is intended for use with '
                        'Python 2.7 or greater.'),
      license='Copyright (C) 2017 by Vinay Sajip. All Rights Reserved. See LICENSE.txt for license.',
      version=__version__,
      author='Vinay Sajip',
      author_email='vinay_sajip@red-dove.com',
      maintainer='Vinay Sajip',
      maintainer_email='vinay_sajip@red-dove.com',
      packages=['tls'],
      # package_data={'tls': ['lib/**/*.*']},
      platforms='No particular restrictions',
      classifiers=[
          'Development Status :: 3 - Alpha',
          'Intended Audience :: Developers',
          'License :: OSI Approved :: BSD License',
          'Programming Language :: Python',
          'Programming Language :: Python :: 2',
          'Programming Language :: Python :: 3',
          'Programming Language :: Python :: 2.7',
          'Programming Language :: Python :: 3.2',
          'Programming Language :: Python :: 3.3',
          'Programming Language :: Python :: 3.4',
          'Programming Language :: Python :: 3.5',
          'Programming Language :: Python :: 3.6',
          'Operating System :: OS Independent',
          'Topic :: Software Development :: Libraries :: Python Modules'
      ]
      )
