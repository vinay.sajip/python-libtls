#
# Copyright 2017 by Vinay Sajip. All Rights Reserved.
#
import argparse
import asyncore
import random
try:
    import socketserver
except ImportError:
    # noinspection PyUnresolvedReferences,PyPep8Naming
    import SocketServer as socketserver
import sys
try:
    import threading
except ImportError:
    threading = None
import unittest
import warnings

try:
    # noinspection PyPackageRequirements
    import h11
except ImportError:  # pragma: no cover
    h11 = None

from tls import *

logger = logging.getLogger(__name__)
HERE = os.path.abspath(os.path.dirname(__file__))
TESTDATA = os.path.join(HERE, 'tests', 'data')
PY3 = sys.version_info[0] >= 3
SSLNotReadyErrors = (SSLWantReadError, SSLWantWriteError)

CERT_DETAILS = {
    'red-dove.com': {
        'issuer': 'Let\'s Encrypt',
        'names': ('red-dove.com', 'www.red-dove.com', 'red-dove.co.uk',
                  'www.red-dove.co.uk'),
        'hash': 'SHA256:fce62d3a8fb68fb224a646b86c093babc493180060de9b3ee21c9434ba31c36e',
        'cipher': 'ECDHE-RSA-AES128-GCM-SHA256',
        'version': 'TLSv1.2',
    },
    'github.com': {
        'issuer': 'DigiCert',
        'names': ('github.com', 'www.github.com'),
        'hash': 'SHA256:25fe3932d9638c8afca19a2987d83e4c1d98db71e41a480398ea226abd8b9316',
        'cipher': 'ECDHE-RSA-AES128-GCM-SHA256',
        'version': 'TLSv1.2',
    },
    'bitbucket.org': {
        'issuer': 'DigiCert',
        'names': ('bitbucket.org', 'www.bitbucket.org'),
        # Sometimes get a wildcard cert with a different hash
        #'hash': 'SHA256:4e653e760f8159855b50060cc24d3c56538b833e9bfa5526989acae225039247',
        'cipher': 'ECDHE-RSA-AES128-GCM-SHA256',
        'version': 'TLSv1.2',
        'require_ocsp_stapling': True,
        'ocsp_url': 'http://ocsp.digicert.com',
    },
}


if threading:
    class ThreadedServer(socketserver.ThreadingTCPServer):
        """
        A basic threaded socket server.
        """

        allow_reuse_address = True

        def __init__(self, addr, handler, poll_interval=0.5,
                     bind_and_activate=True, name=None):

            # noinspection PyClassHasNoInit
            class DelegatingTCPRequestHandler(socketserver.StreamRequestHandler):

                def handle(self):
                    self.server.handler(self)

            socketserver.ThreadingTCPServer.__init__(self, addr,
                                                     DelegatingTCPRequestHandler,
                                                     bind_and_activate)
            self.ctx = None
            # server control
            self._thread = None
            self.poll_interval = poll_interval
            self.handler = handler
            self.ready = threading.Event()
            self.name = name

        def server_bind(self):
            socketserver.ThreadingTCPServer.server_bind(self)
            # noinspection PyAttributeOutsideInit
            self.port = self.socket.getsockname()[1]

        def get_request(self):
            result = socketserver.ThreadingTCPServer.get_request(self)
            if self.ctx:
                sock, addr = result
                wrapped = self.ctx.wrap_socket(sock, server_side=True)
                result = wrapped, addr
            return result

        # server control
        def start(self):
            """
            Create a daemon thread to run the server, and start it.
            """
            self._thread = t = threading.Thread(target=self.serve_forever,
                                                args=(self.poll_interval,))
            t.daemon = True
            if self.name:
                t.name = self.name
            t.start()

        def serve_forever(self, poll_interval=0.5):
            """
            Run the server. Set the ready flag before entering the
            service loop.
            """
            self.ready.set()
            socketserver.ThreadingTCPServer.serve_forever(self, poll_interval)

        def stop(self, timeout=None):
            """
            Tell the server thread to stop, and wait for it to do so.

            :param timeout: How long to wait for the server thread
                            to terminate.
            """
            self.shutdown()
            if self._thread is not None:
                self._thread.join(timeout)
                self._thread = None
            self.server_close()
            self.ready.clear()


class TestDispatcher(asyncore.dispatcher):
    """
    The same as asyncore.dispatcher, but avoids closing the socket on close
    """
    def close(self):
        self.connected = False
        self.accepting = False
        self.connecting = False
        self.del_channel()

    def close_socket(self):
        if self.wrapped:
            self.wrapped.close()
        else:
            self.socket.close()

class AsyncClient(TestDispatcher):
    def __init__(self, host, port, ctx=None):
        TestDispatcher.__init__(self)
        self.host = host
        self.port = port
        self.ctx = ctx
        self.wrapped = None
        self.write_buffer = b''
        self.read_buffer = b''
        self.create_socket(socket.AF_INET, socket.SOCK_STREAM)
        logger.debug('connecting: %s:%d', host, port)
        self.connect((host, port))

    def handle_connect(self):
        if self.ctx:
            try:
                self.wrapped = self.ctx.connect(self.host, self.port, self.socket)
            except SSLNotReadyErrors:
                logger.debug('handle_connect - not ready')

    def handle_close(self):
        logger.debug('handle_close')
        self.close()

    def writable(self):
        return bool(self.write_buffer)

    def readable(self):
        # logger.debug('we are readable')
        return True

    def handle_write(self):
        sent = 0
        if not self.ctx:
            sent = self.send(self.write_buffer)
        elif self.wrapped:
            try:
                sent = self.wrapped.send(self.write_buffer)
            except SSLNotReadyErrors as e:
                pass
        if sent:
            logger.debug('sent: %r', self.write_buffer[:sent])
            self.write_buffer = self.write_buffer[sent:]

    def handle_read(self):
        ctx = self.ctx
        if not ctx:
            data = self.recv(8192)
        else:
            data = b''
            try:
                if self.wrapped:
                    data = self.wrapped.recv(512)
                    if not data:
                        self.handle_close()
                else:
                    self.wrapped = self.ctx.wrap_socket(self.socket)
            except SSLNotReadyErrors as e:
                pass
        if data:
            logger.debug('received: %r', data)
            self.read_buffer += data

    def write(self, data):
        self.write_buffer += data


class AsyncHandler(asyncore.dispatcher):
    def __init__(self, server, sock):
        self.server = server
        self.write_buffer = b''
        asyncore.dispatcher.__init__(self, sock=sock)

    def writable(self):
        return bool(self.write_buffer)

    def handle_write(self):
        sent = self.send(self.write_buffer)
        if sent:
            logger.debug('sent: %r', self.write_buffer[:sent])
            self.write_buffer = self.write_buffer[sent:]
        if not self.writable():
            self.handle_close()
            self.server.handle_close()

    def handle_read(self):
        try:
            data = self.recv(50)
            logger.debug('received: %r', data)
            self.write_buffer += b'Back atcha: ' + data
        except SSLNotReadyErrors:
            pass

    def handle_close(self):
        self.close()


class AsyncServer(TestDispatcher):
    def __init__(self, addr, ctx=None):
        TestDispatcher.__init__(self)
        self.host = addr[0]
        self.ctx = ctx
        self.wrapped = None
        self.create_socket(socket.AF_INET, socket.SOCK_STREAM)
        self.bind(addr)
        self.address = self.socket.getsockname()
        logger.debug('binding to %s', self.address)
        self.port = self.address[1]
        self.listen(5)

    def handle_accept(self):
        sock, addr = self.accept()
        wrapped = self.ctx.wrap_socket(sock, server_side=True)
        AsyncHandler(self, wrapped)

    def handle_close(self):
        self.close()


class BasicTestCase(unittest.TestCase):
    def setUp(self):
        logger.debug('-- S -- %s' % self._testMethodName)

    def tearDown(self):
        logger.debug('-- E -- %s' % self._testMethodName)

    def test_parse_protocols(self):
        cases = (
            ('secure', TLS_PROTOCOL_TLSv1_2),
            ('default', TLS_PROTOCOL_TLSv1_2),
            ('legacy', TLS_PROTOCOLS_ALL),
            ('all', TLS_PROTOCOLS_ALL),
            ('tlsv1.0', TLS_PROTOCOL_TLSv1_0),
            ('tlsv1.1', TLS_PROTOCOL_TLSv1_1),
            ('tlsv1.2', TLS_PROTOCOL_TLSv1_2),
            ('all:!tlsv1.0', TLS_PROTOCOL_TLSv1_1 | TLS_PROTOCOL_TLSv1_2),
        )
        for k, expected in cases:
            actual = parse_protocols(k)
            self.assertEqual(actual, expected)
        self.assertRaises(TLSError, parse_protocols, 'foobar')

    # noinspection PyMethodMayBeStatic
    def get_context(self, **kwargs):
        """
        Get a context. By default, a secure client context is returned.
        """
        return Client(**kwargs)

    def check_connection_info(self, ctx, info):
        rc = ctx.peer_cert_provided
        self.assertEqual(rc, 1)
        nb = ctx.peer_cert_notbefore
        na = ctx.peer_cert_notafter
        self.assertGreater(na, nb)
        issuer = info['issuer']
        subject = info.get('subject')
        names = info.get('names', ())
        cert_hash = info.get('hash')
        if names and not subject:
            subject = names[0]
        self.assertIn(issuer, ctx.peer_cert_issuer)
        if subject:
            cn = '/CN=.*%s' % subject
            self.assertRegexpMatches(ctx.peer_cert_subject, cn)
        if cert_hash is not None:
            self.assertEqual(cert_hash, ctx.peer_cert_hash)
        if 'cipher' in info:
            self.assertEqual(info['cipher'], ctx.conn_cipher)
        if 'version' in info:
            self.assertEqual(info['version'], ctx.conn_version)
        for name in names:
            rc = ctx.peer_cert_contains_name(name)
            self.assertEqual(rc, 1)

    def test_invalid_kwargs(self):
        # not valid for any context ...
        self.assertRaises(ValueError, Client, require_stapling=True)
        self.assertRaises(ValueError, Server, require_stapling=True)

        # not appropriate for client contexts ...
        self.assertRaises(ValueError, Client, verify_client=True)
        self.assertRaises(ValueError, Client, prefer_client_ciphers=True)
        self.assertRaises(ValueError, Client,
                          extra_certificate_key_paths=(None, None))
        self.assertRaises(ValueError, Client, session_id=b'foobar')
        self.assertRaises(ValueError, Client, session_lifetime=10)

        # not appropriate for server contexts ...
        self.assertRaises(ValueError, Server, require_ocsp_stapling=True)
        self.assertRaises(ValueError, Server, verify_cert=False)
        self.assertRaises(ValueError, Server, verify_name=False)
        self.assertRaises(ValueError, Server, verify_time=False)

    def test_invalid_socket(self):
        dummy_sock = argparse.Namespace(fileno=lambda: -1,
                                        close=lambda: None,
                                        getsockname=lambda: ('0.0.0.0', 1))

        sctx, cctx = self.get_server_test_contexts()
        self.assertRaises(TLSError, cctx.connect, 'github.com', 443, dummy_sock)
        # following should raise a TLSError, but doesn't due to a bug in libtls
        self.assertRaises(ValueError, sctx.accept, dummy_sock)

    def test_certificates_no_existing_socket(self):
        for domain, info in CERT_DETAILS.items():
            ctx = self.get_context()
            wrapped = ctx.connect(domain, 443)
            wrapped.do_handshake()
            self.check_connection_info(ctx, info)
            self.assertEqual(ctx.conn_servername, domain)

    def test_certificates_no_existing_socket_with_hostname(self):
        for domain, info in CERT_DETAILS.items():
            ctx = self.get_context()
            hostname = random.choice(info['names'])
            wrapped = ctx.connect(domain, 443, hostname=hostname)
            wrapped.do_handshake()
            self.check_connection_info(ctx, info)
            self.assertEqual(ctx.conn_servername, hostname)

    def test_ocsp_status(self):
        for domain, info in CERT_DETAILS.items():
            stapling = info.get('require_ocsp_stapling', False)
            ctx = self.get_context(require_ocsp_stapling=stapling)
            wrapped = ctx.connect(domain, 443)
            wrapped.do_handshake()
            self.check_connection_info(ctx, info)
            if stapling:
                self.assertEqual(ctx.peer_ocsp_response_status, 0)
                self.assertEqual(ctx.peer_ocsp_cert_status, 0)
                self.assertEqual(ctx.peer_ocsp_url, info['ocsp_url'])
                self.assertEqual(ctx.peer_ocsp_result, 'good')
                tu = ctx.peer_ocsp_this_update
                nu = ctx.peer_ocsp_next_update
                self.assertIsNotNone(tu)
                self.assertIsNotNone(nu)
                self.assertGreater(nu, tu)
                self.assertIsNotNone(ctx.peer_ocsp_revocation_time)
                self.assertEqual(ctx.peer_ocsp_crl_reason, 0)

    # noinspection PyMethodMayBeStatic
    def get_connected_socket(self, ctx, domain, port=443):
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        logger.debug('created socket: %d', sock.fileno())
        return ctx.connect(domain, port, sock)

    def test_certificates_existing_socket(self):
        for domain, info in CERT_DETAILS.items():
            ctx = self.get_context()
            sock = self.get_connected_socket(ctx, domain)
            sock.do_handshake()
            self.check_connection_info(ctx, info)
            self.assertEqual(ctx.conn_servername, domain)
            ctx.close()
            sock.close()

    @unittest.skipUnless(h11, 'The h11 library is needed for this test')
    def test_http_client(self):
        domain = 'red-dove.com'
        # info = CERT_DETAILS[domain]
        ctx = self.get_context()
        sock = self.get_connected_socket(ctx, domain)
        # Prepare a request
        # noinspection PyUnresolvedReferences
        conn = h11.Connection(our_role=h11.CLIENT)
        req = h11.Request(method='GET', target='/', headers=[('Host', domain)])
        data = conn.send(req)
        sock.sendall(data)
        # Get the response
        data = sock.recv(512)
        e = conn.next_event()
        self.assertIs(e, h11.NEED_DATA)
        conn.receive_data(data)
        e = conn.next_event()
        self.assertIsInstance(e, h11.Response)
        # noinspection PyUnresolvedReferences
        self.assertEqual(e.status_code, 301)
        d = dict(e.headers)
        self.assertEqual(d[b'location'], b'https://www.red-dove.com/')
        e = conn.next_event()
        self.assertIsInstance(e, h11.Data)
        e = conn.next_event()
        self.assertIsInstance(e, h11.EndOfMessage)
        ctx.close()
        sock.close()

    def get_server_test_contexts(self):
        certpath = os.path.join(TESTDATA, 'servercert.pem')
        keypath = os.path.join(TESTDATA, 'serverkey.pem')
        capath = os.path.join(TESTDATA, 'cacert.pem')
        sctx = Server(cafile=capath, keypair_paths=(certpath, keypath))
        # Prevent errors on self-signed certificate and because we're using
        # localhost for the server
        cctx = self.get_context(verify_cert=False, verify_name=False)
        return sctx, cctx

    @unittest.skipUnless(threading, 'The threading module is needed for this test')
    def test_http_server(self):
        sctx, cctx = self.get_server_test_contexts()
        addr = ('localhost', 0)
        server = ThreadedServer(addr, self.handle_socket_request, 0.01,
                                name='ServerThread')
        server.ctx = sctx
        server.start()
        self.addCleanup(server.stop, 2.0)
        server.ready.wait()
        # Now, server.port is the port to connect to
        # let's try connecting
        sock = self.get_connected_socket(cctx, 'localhost', server.port)
        sock.sendall(b'Hello, world!')
        data = sock.recv(50)
        self.assertEqual(data, b'Back atcha: Hello, world!')
        sctx.close()
        cctx.close()
        sock.close()

    def handle_socket_request(self, request):
        sock = request.connection
        self.assertIsInstance(sock, TLSSocket)
        data = sock.recv(4096)
        data = b'Back atcha: ' + data
        sock.sendall(data)

    @unittest.skipUnless(h11, 'The h11 library is needed for this test')
    def test_async_http_client(self):
        host = 'red-dove.com'
        port = 443
        ctx = self.get_context()
        client = AsyncClient(host, port, ctx)
        conn = h11.Connection(our_role=h11.CLIENT)
        req = h11.Request(method='GET', target='/', headers=[('Host', host)])
        data = conn.send(req)
        client.write(data)
        asyncore.loop()
        self.check_connection_info(ctx, CERT_DETAILS[host])
        ctx.close()
        client.close_socket()

    def test_async_http_server(self):
        sctx, cctx = self.get_server_test_contexts()
        host = 'localhost'
        addr = (host, 0)
        server = AsyncServer(addr, sctx)
        client = AsyncClient(host, server.port, cctx)
        client.write(b'Hello, world!')
        asyncore.loop()
        self.assertEqual(client.read_buffer, b'Back atcha: Hello, world!')
        cctx.close()
        client.close_socket()
        sctx.close()
        server.close_socket()


# noinspection PyUnusedLocal
def suite(args=None):
    result = unittest.TestLoader().loadTestsFromTestCase(BasicTestCase)
    return result


def init_logging():
    f = '%(asctime)s %(levelname)-5s %(name)s %(threadName)s %(lineno)4d %(message)s'
    logging.basicConfig(level=logging.DEBUG, filename='test_tls.log', filemode="w", format=f)


def main(args=None):
    parser = argparse.ArgumentParser()
    parser.add_argument('-v', '--verbose', default=1, type=int,
                        help='Verbose output')
    parser.add_argument('-f', '--failfast', default=False, action='store_true',
                        help='Stop on first failure')
    options = parser.parse_args(args)
    init_logging()
    logging.captureWarnings(True)
    if sys.version_info[:2] >= (3, 2):
        warnings.simplefilter('always', ResourceWarning)
    logger.debug('Using system tlslib: %s', LIBTLS_SYSTEM)
    tests = suite()
    runner = unittest.TextTestRunner(verbosity=options.verbose,
                                     failfast=options.failfast)
    results = runner.run(tests)
    return not results.wasSuccessful()


if __name__ == "__main__":
    sys.exit(main())
