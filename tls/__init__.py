#
# Copyright 2017 by Vinay Sajip. All Rights Reserved.
#
import datetime
from errno import EWOULDBLOCK
import logging
from io import BlockingIOError
import os
import socket
from ssl import SSLWantReadError, SSLWantWriteError

class TLSError(Exception):
    pass

class ConfigurationError(TLSError):
    pass

class ConnectionError(TLSError):
    pass

class VerificationError(ConnectionError):
    pass

def _return_error(e):
    if not isinstance(e, (OSError, AttributeError)):
        return e
    correct = detail = ''
    if isinstance(e, AttributeError):
        for attr in ('kind', 'library', 'name'):
            if not hasattr(e, attr):
                return e
        detail = ('The %s "%s" was not available in library %s.' % (e.kind,
                  e.name, e.library._name))
        correct = 'correct '
    elif getattr(e, 'winerror', None) == 193:
        detail = 'The library found is not compatible with this Python.'
        correct = 'correct '
    parts = ['Unable to load the %slibtls shared library.' % correct]
    if detail:
        parts.append(detail)
    if os.name == 'nt':
        msg = 'PATH'
    else:
        msg = 'LD_LIBRARY_PATH'
    parts.append('Try setting environment variable %s to include '
                 'the location of the %sshared library.' % (msg, correct))
    msg = '\n'.join(parts)
    e = ImportError(msg)
    e.__context__ = None
    e.__cause__ = None
    return e

try:
    from ._tls import *
except (OSError, AttributeError) as e:
    e = _return_error(e)
    raise e

logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())


_fsencode = getattr(os, 'fsencode', None)
_to_datetime = lambda t: datetime.datetime.utcfromtimestamp(t)

UNCONNECTED = ('0.0.0.0', 0)


def _init():
    rc = tls_init()
    if rc:  # pragma: no cover
        raise ConfigurationError('Unable to initialise libtls: %s' % rc)

_init()
del _init


def _to_string(b, enc='utf-8'):
    return b if b is None else b.decode(enc)

def parse_protocols(s):
    """
    Parse a protocol string and return the protocols represented.
    """
    result = uint32_t(0)
    b = s.encode('ascii')  # assumed to be good enough
    rc = tls_config_parse_protocols(ctypes.byref(result), b)
    if rc:
        raise ConfigurationError('unable to parse protocols %s' % s)
    return result.value


def dispose_context(context):
    # logger.debug('dispose context: %s', context)
    # If for any reason a socket connected to this context has been closed,
    # calling tls_close will cause an exception on Windows (though not on
    # POSIX) - so avoid calling it.
    # tls_close(context)
    tls_free(context)


def config_error(config):
    return _to_string(tls_config_error(config))


def context_error(context):
    return _to_string(tls_error(context))


class TLSSocket(object):
    """
    This class wraps a socket and its libtls context together, such that
    I/O uses the context rather than the underlying socket.
    """

    def __init__(self, context, sock, own_context=False):
        self.context = context
        self.sock = sock
        self.own_context = own_context
        self.handshaken = False

    def __repr__(self):
        sock = self.sock
        if sock:
            sock = sock.fileno()
        return '%s(%s, %s, %s)' % (self.__class__.__name__, self.context, sock,
                                       self.own_context)


    def __getattr__(self, name):
        if not self.sock:
            raise AttributeError('no underlying socket')
        return getattr(self.sock, name)

    def close(self):
        if self.own_context and self.context:
            dispose_context(self.context)
            self.context = None
        if self.sock:
            logger.debug('closing socket: fd=%d', self.sock.fileno())
            self.sock.close()
            self.sock = None

    __del__ = close

    def recv(self, bufsize, flags=0):
        if bufsize <= 0:
            raise ConfigurationError('invalid buffer size: %d' % bufsize)
        if flags:
            raise NotImplementedError('flags not implemented')
        buf = (c_ubyte * bufsize)()
        context = self.context
        read = tls_read(context, buf, bufsize)
        if read == TLS_WANT_POLLIN:
            raise SSLWantReadError(EWOULDBLOCK)
        if read == TLS_WANT_POLLOUT:
            raise SSLWantWriteError(EWOULDBLOCK)
        if read < 0:
            raise ConnectionError('unable to read: %s' % context_error(context))
        return memoryview(buf)[:read].tobytes()

    def send(self, data, flags=0):
        if flags:
            raise NotImplementedError('flags not implemented')
        n = len(data)
        result = 0
        context = self.context
        while n > 0:
            written = tls_write(context, data, n)
            if written >= 0:
                result = written
                break
            if written == TLS_WANT_POLLIN:
                raise SSLWantReadError(EWOULDBLOCK)
            if written == TLS_WANT_POLLOUT:
                raise SSLWantWriteError(EWOULDBLOCK)
            if written < 0:
                raise ConnectionError('unable to write: %s' %
                                      context_error(context))
        return result

    def sendall(self, data, flags=0):
        n = len(data)
        while n > 0:
            written = self.send(data, flags)
            n -= written
            data = data[written:]

    # def __enter__(self, *args, **kwargs):
        # return self.sock.__enter__(*args, **kwargs)

    # def __exit__(self, *args, **kwargs):
        # return self.sock.__exit__(*args, **kwargs)

    def do_handshake(self):
        if self.handshaken:
            return
        context = self.context
        logger.debug('initiating handshake: %s', context)
        rc = tls_handshake(context)
        if rc == TLS_WANT_POLLIN:
            raise SSLWantReadError(EWOULDBLOCK)
        if rc == TLS_WANT_POLLOUT:
            raise SSLWantWriteError(EWOULDBLOCK)
        if rc:
            raise VerificationError('handshake failed: %s' %
                                    context_error(context))
        logger.debug('handshake OK')
        self.handshaken = True

class _Context(object):
    """
    This class wraps a libtls configuration and context.
    """

    wrapper_class = TLSSocket

    def __init__(self, **kwargs):
        self._config = config = tls_config_new()
        # logger.debug('new config: %s', config)
        self._context = self.host = self.port = self.sock = None
        server = kwargs.pop('server', False)
        if server:
            # server config items

            verify_client = kwargs.pop('verify_client', False)
            if verify_client:
                tls_config_verify_client(config)
            else:
                tls_config_verify_client_optional(config)

            # ocsp_staple_file = kwargs.pop('ocsp_staple_file', None)
            # if ocsp_staple_file:
                # if _fsencode:
                    # ocsp_staple_file = _fsencode(ocsp_staple_file)
                    # rc = tls_config_set_ocsp_staple_file(ocsp_staple_file)
                    # if rc:
                        # raise ConfigurationError('unable to set OCSP staple '
                                                 # 'file: %s' %
                                                 # config_error(config))

            prefer_client_ciphers = kwargs.pop('prefer_client_ciphers', False)
            if prefer_client_ciphers:
                tls_config_prefer_ciphers_client(config)

            session_id = kwargs.pop('session_id', None)
            if session_id:
                if not instance(session_id, bytes):
                    raise TypeError('session id provided was not '
                                    'a bytestring: %s' % session_id)
                rc = tls_config_set_session_id(config, session_id,
                                               len(session_id))
                if rc:
                    raise ConfigurationError('unable to set session id: %s' %
                                             config_error(config))

            session_lifetime = kwargs.pop('session_lifetime', None)
            if session_lifetime is not None:
                rc = tls_config_set_session_lifetime(config, session_lifetime)
                if rc:
                    raise ConfigurationError('unable to set session lifetime: '
                                             '%s' % config_error(config))

        else:
            # client config items

            verify_cert = kwargs.pop('verify_cert', True)
            if not verify_cert:
                tls_config_insecure_noverifycert(config)

            verify_name = kwargs.pop('verify_name', True)
            if not verify_name:
                tls_config_insecure_noverifyname(config)

            verify_time = kwargs.pop('verify_time', True)
            if not verify_time:
                tls_config_insecure_noverifytime(config)

            require_ocsp_stapling = kwargs.pop('require_ocsp_stapling', False)
            if require_ocsp_stapling:
                tls_config_ocsp_require_stapling(config)

        # common to client and server

        cafile = kwargs.pop('cafile', None)
        # if not using a "system" libtls, point to a local cert.pem
        # if not server and cafile is None and not LIBTLS_SYSTEM:
            # here = os.path.abspath(os.path.dirname(__file__))
            # cafile = os.path.join(here, 'lib', 'cert.pem')
        if cafile:
            if _fsencode:
                cafile = _fsencode(cafile)
            rc = tls_config_set_ca_file(config, cafile)
            if rc:
                raise ConfigurationError('unable to set CA file: %s' %
                                         config_error(config))

        capath = kwargs.pop('capath', None)
        # if not using a "system" libtls, point to a local directory
        # with certificates in PEM format.
        if capath:
            if not os.path.isdir(capath):
                raise ValueError('not a directory: %s' % capath)
            if _fsencode:
                capath = _fsencode(capath)
            rc = tls_config_set_ca_path(config, capath)
            if rc:
                raise ConfigurationError('unable to set CA path: %s' %
                                         config_error(config))

        def validate_keypair_paths(keypair_paths):
            """
            A utility function to validate and pre-process keypair path data
            """
            if not isinstance(keypair_paths, (list, tuple)):
                raise ValueError('keypair_paths is not a list or tuple')
            n = len(keypair_paths)
            if n == 3:
                certpath, keypath, ocsppath = keypair_paths
                if not os.path.isfile(ocsppath):
                    raise ValueError('not a file: %s' % ocsppath)
            elif n == 2:
                certpath, keypath = keypair_paths
                ocsppath = None
            else:
                raise ValueError('not a valid length for keypair_paths: %d' % n)

            if not os.path.isfile(certpath):
                raise ValueError('not a file: %s' % certpath)
            if not os.path.isfile(keypath):
                raise ValueError('not a file: %s' % keypath)
            if _fsencode:
                certpath = _fsencode(certpath)
                keypath = _fsencode(keypath)
                if ocsppath:
                    ocsppath = _fsencode(ocsppath)
            return certpath, keypath, ocsppath

        keypair_paths = kwargs.pop('keypair_paths', None)
        if keypair_paths:
            certpath, keypath, ocsppath = validate_keypair_paths(keypair_paths)
            if not ocsppath:
                rc = tls_config_set_keypair_file(config, certpath, keypath)
            else:
                rc = tls_config_set_keypair_ocsp_file(config, certpath, keypath,
                                                      ocsppath)
            if rc:
                raise ConfigurationError('unable to set keypair paths: %s' %
                                         config_error(config))

        verify_depth = kwargs.pop('verify_depth', None)
        if verify_depth is not None:
            rc = tls_config_set_verify_depth(verify_depth)
            if rc:
                raise ConfigurationError('unable to set verify depth: %s' %
                                         config_error(config))

        ciphers = kwargs.pop('ciphers', None)
        if ciphers:
            rc = tls_config_set_ciphers(ciphers.encode('ascii'))
            if rc:
                raise ConfigurationError('unable to set ciphers: %s' %
                                         config_error(config))

        dhe_params = kwargs.pop('dhe_params', None)
        if dhe_params:
            rc = tls_config_set_dheparams(dhe_params.encode('ascii'))
            if rc:
                raise ConfigurationError('unable to set DHE params: %s' %
                                         config_error(config))

        ecdhe_curve = kwargs.pop('ecdhe_curve', None)
        if ecdhe_curve:
            rc = tls_config_set_ecdhecurve(ecdhe_curve.encode('ascii'))
            if rc:
                raise ConfigurationError('unable to set ECDHE curve: %s' %
                                         config_error(config))

        alpn = kwargs.pop('alpn', None)
        if alpn:
            rc = tls_config_set_alpn(alpn.encode('ascii'))
            if rc:
                raise ConfigurationError('unable to set ALPN: %s' %
                                         config_error(config))

        # do any additional certificate/key pairs here for servers,
        # after tls_config_set_keypair_file has been called.

        if server:
            extra_keypairs = kwargs.pop('extra_certificate_key_paths', [])
            for kpp in extra_keypairs:
                certpath, keypath, ocsppath = validate_keypair_paths(kpp)
                if not ocsppath:
                    rc = tls_config_add_keypair_file(config, certpath, keypath)
                else:
                    rc = tls_config_add_keypair_ocsp_file(config, certpath,
                                                          keypath, ocsppath)
                if rc:
                    raise ConfigurationError('unable to set additional '
                                             'certificate-key pair paths '
                                             '%s: %s' %
                                             (kpp, config_error(config)))

        # if any kwargs left over, there could be a typo ...
        if kwargs:
            raise ValueError('unrecognised keyword arguments: %s' %
                             ', '.join(kwargs))

        if server:
            context = tls_server()
        else:
            context = tls_client()
        # logger.debug('new context: %s', context)
        self._context = context
        rc = tls_configure(context, config)
        if rc:
            raise ConfigurationError('unable to configure context: %s' %
                                     context_error(context))
        self.server = server
        self.connected = False

    def close(self):
        context = self._context
        if context:
            dispose_context(context)
            self._context = None
        config = self._config
        if config:
            tls_config_free(config)
            self._config = None

    __del__ = close

    @property
    def peer_cert_provided(self):
        return bool(tls_peer_cert_provided(self._context))

    @property
    def peer_cert_notbefore(self):
        t = tls_peer_cert_notbefore(self._context)
        return _to_datetime(t)

    @property
    def peer_cert_notafter(self):
        t = tls_peer_cert_notafter(self._context)
        return _to_datetime(t)

    @property
    def peer_cert_issuer(self):
        return _to_string(tls_peer_cert_issuer(self._context))

    @property
    def peer_cert_subject(self):
        return _to_string(tls_peer_cert_subject(self._context))

    @property
    def peer_cert_hash(self):
        return _to_string(tls_peer_cert_hash(self._context))

    @property
    def peer_ocsp_response_status(self):
        return tls_peer_ocsp_response_status(self._context)

    @property
    def peer_ocsp_cert_status(self):
        return tls_peer_ocsp_cert_status(self._context)

    @property
    def peer_ocsp_crl_reason(self):
        return tls_peer_ocsp_crl_reason(self._context)

    @property
    def peer_ocsp_this_update(self):
        return _to_datetime(tls_peer_ocsp_this_update(self._context))

    @property
    def peer_ocsp_next_update(self):
        return _to_datetime(tls_peer_ocsp_next_update(self._context))

    @property
    def peer_ocsp_revocation_time(self):
        return _to_datetime(tls_peer_ocsp_revocation_time(self._context))

    @property
    def peer_ocsp_url(self):
        return _to_string(tls_peer_ocsp_url(self._context), 'idna')

    @property
    def peer_ocsp_result(self):
        return _to_string(tls_peer_ocsp_result(self._context))

    @property
    def conn_servername(self):
        return _to_string(tls_conn_servername(self._context), 'idna')

    @property
    def conn_cipher(self):
        return _to_string(tls_conn_cipher(self._context), 'ascii')

    @property
    def conn_version(self):
        return _to_string(tls_conn_version(self._context), 'ascii')

    @property
    def conn_alpn_selected(self):
        return _t_string(tls_conn_alpn_selected(self._context), 'ascii')

    @property
    def context_error(self):
        return context_error(self._context)

    @property
    def config_error(self):
        return config_error(self._config)

    def peer_cert_contains_name(self, name):
        return bool(tls_peer_cert_contains_name(self._context,
                                                name.encode('idna')))

    def process_ocsp_response(self, response):
        return tls_ocsp_process_response(self._context)

    if False:  # disabled for now
        def check_fds(self, read_fd, write_fd):
            if read_fd < 0:
                raise ValueError('invalid read descriptor: %d' % read_fd)
            if write_fd < 0:
                raise ValueError('invalid write descriptor: %d' % write_fd)


class Client(_Context):
    def __init__(self, **kwargs):
        kwargs['server'] = False
        super(Client, self).__init__(**kwargs)

    def _ensure_connected(self, sock, hostname=None):
        if not self.connected:
            host = self.host
            port = self.port
            context = self._context
            try:
                sockname = sock.getsockname()
            except socket.error as e:
                # on Windows, we get a socket error when not connected
                # with error code 10022
                if os.name != 'nt':
                    raise
                if e.errno != 10022:
                    raise
                sockname = UNCONNECTED
            if sockname == UNCONNECTED:
                sock.connect((host, port))
            if hostname is None:
                hostname = host
            rc = tls_connect_socket(context, sock.fileno(), hostname.encode('idna'))
            if rc:
                raise ConnectionError('unable to connect socket: %s:%d: %s' %
                                      (host, port, context_error(context)))
            self.connected = True

    def connect(self, host, port, sock=None, hostname=None):
        if self.server:
            raise ValueError('you cannot connect a socket '
                             'using a server context')
        context = self._context
        self.host = host
        self.port = port
        self.sock = sock
        if not sock:
            host_port = host.encode('idna') + (':%d' % port).encode('ascii')
            if not hostname:
                rc = tls_connect(context, host_port, None)
            else:
                hostname = hostname.encode('idna')
                rc = tls_connect_servername(context, host_port, None, hostname)
            if rc:
                raise ConnectionError('unable to connect %s:%d: %s' %
                                      (host, port, context_error(context)))
            self.connected = True

        return self.wrap_socket(sock, server_hostname=hostname)

    def wrap_socket(self, sock, **kwargs):
        if sock:
            hostname = kwargs.get('server_hostname') or self.host
            self._ensure_connected(sock, hostname)
        return self.wrapper_class(self._context, sock)

    if False:  # disabled for now
        def connect_fds(self, read_fd, write_fd, hostname):
            if self.server:
                raise ValueError('you cannot connect a socket '
                                 'using a server context')
            self.check_fds(read_fd, write_fd)
            if not hostname:
                raise ValueError('invalid hostname: %s' % hostname)
            ehostname = hostname.encode('idna')
            context = self._context
            rc = tls_connect_fds(context, read_fd, write_fd, ehostname)
            if rc:
                raise ConnectionError('unable to connect fds (%d, %d): %s' %
                                      (read_fd, write_fd,
                                       context_error(context)))
            self.connected = True
            return self.wrapper_class(self._context, None)


class Server(_Context):
    def __init__(self, **kwargs):
        kwargs['server'] = True
        super(Server, self).__init__(**kwargs)

    def accept(self, sock):
        fd = sock.fileno()
        if fd < 0:
            raise ValueError('invalid socket descriptor: %d' % fd)
        new_context = c_void_p()
        context = self._context
        rc = tls_accept_socket(context, ctypes.byref(new_context), fd)
        if rc:
            raise ConnectionError('unable to accept socket: %s' %
                                  context_error(context))
        logger.debug('new accepted socket: %s, %d', new_context, fd)
        result = self.wrapper_class(new_context, sock, own_context=True)
        self.connected = True
        return result

    def wrap_socket(self, sock, **kwargs):
        """
        Provided for compatibility with other contexts. kwargs is ignored,
        would normally contain server_side=True for a server-side context
        """
        return self.accept(sock)

    if False:  # disabled for now
        def accept_fds(self, read_fd, write_fd):
            if not self.server:
                raise ValueError('you cannot accept a socket '
                                 'using a client context')
            self.check_fds(read_fd, write_fd)
            new_context = c_void_p()
            context = self._context
            rc = tls_accept_fds(context, ctypes.byref(new_context), read_fd,
                                write_fd)
            if rc:
                raise ConnectionError('unable to accept fds (%d, %d): %s' %
                                      read_fd, write_fd, context_error(context))
            result = self.wrapper_class(new_context, None, own_context=True)
            self.connected = True
            return result
