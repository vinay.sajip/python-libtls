#
# Copyright 2017 by Vinay Sajip. All Rights Reserved.
#
import ctypes
import ctypes.util
import os
import sys

from ctypes import (c_void_p, c_char_p, c_wchar_p, c_ushort,
                    c_int, c_uint, c_long, c_ulong, c_longlong, c_ulonglong,
                    c_float, c_double, c_longdouble, c_char, c_bool,
                    c_ubyte)

LIBTLS_VERSION = (2, 5, 1)
LIBTLS_REVISION = 'b67d365454e3d35f775dda738697615094e81c4e'

INT_TYPES = {c_ubyte, c_int, c_uint, c_long, c_ulong, c_longlong, c_ulonglong}

_gpt = ctypes.POINTER
_gft = ctypes.CFUNCTYPE

p_ubyte = _gpt(c_ubyte)
p_void_p = _gpt(c_void_p)
p_char_p = _gpt(c_char_p)
p_ushort = _gpt(c_ushort)
p_int = _gpt(c_int)
p_uint = _gpt(c_uint)
p_long = _gpt(c_long)
p_ulonglong = _gpt(c_ulonglong)

_SEARCH_LOCALLY = False

def _load_library(libname, dependencies=None):
    import os.path
    global LIBTLS_SYSTEM

    def ensure_suffixed(name):
        n, e = os.path.splitext(name)
        if not e:
            if os.name == 'nt':
                suffix = '.dll'
            elif sys.platform == 'darwin':
                suffix = '.dylib'
            else:
                suffix = '.so'
            name += suffix
        return name

    def load_library(path):
        # just call ctypes to do it, but provide more information in the error
        # message if not loadable
        try:
            return ctypes.cdll.LoadLibrary(path)
        except OSError as e:  # pragma: no cover
            msg = '%s: %s' % (path, e)
            raise type(e)(msg)

    libname = ensure_suffixed(libname)
    try:
        # See if it's available in a well-known system location
        result = ctypes.cdll.LoadLibrary(libname)
        LIBTLS_SYSTEM = True
        return result
    except OSError:
        # Failed to find it - look for it locally to this package, if
        # so configured
        if not _SEARCH_LOCALLY:
            raise
        from distutils.util import get_platform

        LIBTLS_SYSTEM = False
        libpath = os.path.dirname(os.path.abspath(__file__))
        pname = get_platform()
        libpath = os.path.join(libpath, 'lib', pname)
        if dependencies:
            for d in dependencies:
                d = ensure_suffixed(d)
                p = os.path.join(libpath, d)
                load_library(p)
        p = os.path.join(libpath, libname)
        try:
            return ctypes.cdll.LoadLibrary(p)
        except OSError as e:  # pragma: no cover
            msg = '%s: %s' % (p, e)
            raise type(e)(msg)

if os.name == 'nt':
    LIBTLS_NAME = 'tls-15'
    LIBSSL_NAME = 'ssl-43'
    LIBCRYPTO_NAME = 'crypto-41'
else:
    LIBTLS_NAME = 'libtls'
    LIBSSL_NAME = 'libssl'
    LIBCRYPTO_NAME = 'libcrypto'

_lib = _load_library(LIBTLS_NAME, [LIBCRYPTO_NAME, LIBSSL_NAME])
del _load_library

def _cv(rv, t):
    if t is None:
        rv = None
    elif t in (c_char_p, c_wchar_p):
        rv = t(rv).value
    elif rv is not None and t in (c_void_p,):
        rv = t(rv)
    if rv is None and t in INT_TYPES:
        rv = 0
    return rv


def _get_bytes(p, n):
    assert type(p) is p_ubyte, 'A pointer to an unsigned byte was not passed'
    assert n > 0, 'A positive length was not passed'
    data = ctypes.cast(p, _gpt(c_ubyte * n))
    return (c_char * n).from_buffer(data.contents).raw


def _inc_pointer(p, n):
    pv = ctypes.cast(p, c_void_p)
    m = n * ctypes.sizeof(p.contents)
    return ctypes.cast(pv.value + m, type(p))


# noinspection PyUnusedLocal
def _convert_args(args, argtypes):
    result = []
    # n = len(argtypes)
    for i, arg in enumerate(args):
        if isinstance(arg, memoryview):
            arg = arg.tobytes()
        # ~ if i < n:
        # ~ if isinstance(arg, string_type):
        # ~ t = argtypes[i]
        # ~ if issubclass(t, c_id_p):
        # ~ arg = string_to_native(arg)
        result.append(arg)
    return result


# noinspection PyPep8Naming
class _w(object):
    def __init__(self, t, func):
        self.t = t
        self.func = func

    def __call__(self, *args):
        if args and hasattr(self.func, 'argtypes'):
            args = _convert_args(args, self.func.argtypes)
        rv = self.func(*args)
        rv = _cv(rv, self.t)
        return rv


# noinspection PyPep8Naming
class _fw(_w):
    def __init__(self, lib, nm, rt, at):
        try:
            f = getattr(lib, nm)
            f.restype = rt
            f.argtypes = at
        except AttributeError:  # pragma: no cover
            e = AttributeError('Function %r not available in %s' % (nm, lib))
            e.kind = 'function'
            e.library = lib
            e.name = nm
            e.__context__ = None
            raise e
        _w.__init__(self, rt, f)


# noinspection PyPep8Naming
class _vw(_w):
    def __init__(self, lib, nm, t):
        _w.__init__(self, t, self.get)
        self.lib = lib
        self.nm = nm

    def get(self, *args):
        assert not args
        try:
            return self.t.in_dll(self.lib, self.nm)
        except AttributeError:  # pragma: no cover
            e = AttributeError('Variable %r not available in %s' % (nm, lib))
            e.kind = 'variable'
            e.library = lib
            e.name = nm
            e.__context__ = None
            raise e


size_t = c_ulong

uint8_t = c_ubyte

uint32_t = c_uint

__time_t = c_long

__ssize_t = c_long

ssize_t = __ssize_t

time_t = __time_t

TLS_API = 20170126

TLS_PROTOCOL_TLSv1_0 = (1 << 1)

TLS_PROTOCOL_TLSv1_1 = (1 << 2)

TLS_PROTOCOL_TLSv1_2 = (1 << 3)

TLS_PROTOCOL_TLSv1 = (TLS_PROTOCOL_TLSv1_0 | TLS_PROTOCOL_TLSv1_1 | TLS_PROTOCOL_TLSv1_2)

TLS_PROTOCOLS_ALL = TLS_PROTOCOL_TLSv1

TLS_PROTOCOLS_DEFAULT = TLS_PROTOCOL_TLSv1_2

TLS_WANT_POLLIN = -2

TLS_WANT_POLLOUT = -3

TLS_OCSP_RESPONSE_SUCCESSFUL = 0

TLS_OCSP_RESPONSE_MALFORMED = 1

TLS_OCSP_RESPONSE_INTERNALERROR = 2

TLS_OCSP_RESPONSE_TRYLATER = 3

TLS_OCSP_RESPONSE_SIGREQUIRED = 4

TLS_OCSP_RESPONSE_UNAUTHORIZED = 5

TLS_OCSP_CERT_GOOD = 0

TLS_OCSP_CERT_REVOKED = 1

TLS_OCSP_CERT_UNKNOWN = 2

TLS_CRL_REASON_UNSPECIFIED = 0

TLS_CRL_REASON_KEY_COMPROMISE = 1

TLS_CRL_REASON_CA_COMPROMISE = 2

TLS_CRL_REASON_AFFILIATION_CHANGED = 3

TLS_CRL_REASON_SUPERSEDED = 4

TLS_CRL_REASON_CESSATION_OF_OPERATION = 5

TLS_CRL_REASON_CERTIFICATE_HOLD = 6

TLS_CRL_REASON_REMOVE_FROM_CRL = 8

TLS_CRL_REASON_PRIVILEGE_WITHDRAWN = 9

TLS_CRL_REASON_AA_COMPROMISE = 10

TLS_MAX_SESSION_ID_LENGTH = 32

TLS_TICKET_KEY_SIZE = 48

tls_read_cb = _gft(ssize_t, c_void_p, c_void_p, size_t, c_void_p)
tls_write_cb = _gft(ssize_t, c_void_p, c_void_p, size_t, c_void_p)
tls_init = _fw(_lib, 'tls_init', c_int, [])

tls_config_error = _fw(_lib, 'tls_config_error', c_char_p, [c_void_p])

tls_error = _fw(_lib, 'tls_error', c_char_p, [c_void_p])

tls_config_new = _fw(_lib, 'tls_config_new', c_void_p, [])

tls_config_free = _fw(_lib, 'tls_config_free', None, [c_void_p])

tls_config_add_keypair_file = _fw(_lib, 'tls_config_add_keypair_file', c_int,
                                  [c_void_p, c_char_p, c_char_p])

# tls_config_add_keypair_mem = _fw(_lib, 'tls_config_add_keypair_mem', c_int,
                                 # [c_void_p, _gpt(uint8_t), size_t,
                                  # _gpt(uint8_t), size_t])

tls_config_add_keypair_ocsp_file = _fw(_lib, 'tls_config_add_keypair_ocsp_file',
                                       c_int, [c_void_p, c_char_p, c_char_p,
                                               c_char_p])

# tls_config_add_keypair_ocsp_mem = _fw(_lib, 'tls_config_add_keypair_ocsp_mem',
                                      # c_int, [c_void_p, _gpt(uint8_t), size_t,
                                              # _gpt(uint8_t), size_t,
                                              # _gpt(uint8_t), size_t])

tls_config_set_alpn = _fw(_lib, 'tls_config_set_alpn', c_int, [c_void_p, c_char_p])

tls_config_set_ca_file = _fw(_lib, 'tls_config_set_ca_file', c_int, [c_void_p, c_char_p])

tls_config_set_ca_path = _fw(_lib, 'tls_config_set_ca_path', c_int, [c_void_p, c_char_p])

# tls_config_set_ca_mem = _fw(_lib, 'tls_config_set_ca_mem', c_int, [c_void_p, _gpt(uint8_t), size_t])

tls_config_set_cert_file = _fw(_lib, 'tls_config_set_cert_file', c_int, [c_void_p, c_char_p])

# tls_config_set_cert_mem = _fw(_lib, 'tls_config_set_cert_mem', c_int, [c_void_p, _gpt(uint8_t), size_t])

tls_config_set_ciphers = _fw(_lib, 'tls_config_set_ciphers', c_int, [c_void_p, c_char_p])

tls_config_set_dheparams = _fw(_lib, 'tls_config_set_dheparams', c_int, [c_void_p, c_char_p])

tls_config_set_ecdhecurve = _fw(_lib, 'tls_config_set_ecdhecurve', c_int, [c_void_p, c_char_p])

tls_config_set_key_file = _fw(_lib, 'tls_config_set_key_file', c_int, [c_void_p, c_char_p])

# tls_config_set_key_mem = _fw(_lib, 'tls_config_set_key_mem', c_int,
                             # [c_void_p, _gpt(uint8_t), size_t])

tls_config_set_keypair_file = _fw(_lib, 'tls_config_set_keypair_file', c_int, [c_void_p, c_char_p, c_char_p])

# tls_config_set_keypair_mem = _fw(_lib, 'tls_config_set_keypair_mem', c_int,
                                 # [c_void_p, _gpt(uint8_t), size_t,
                                 # _gpt(uint8_t), size_t])

tls_config_set_keypair_ocsp_file = _fw(_lib, 'tls_config_set_keypair_ocsp_file',
                                       c_int, [c_void_p, c_char_p, c_char_p,
                                       c_char_p])

# tls_config_set_keypair_ocsp_mem = _fw(_lib, 'tls_config_set_keypair_ocsp_mem',
                                      # c_int, [c_void_p, _gpt(uint8_t), size_t,
                                              # _gpt(uint8_t), size_t,
                                              # _gpt(uint8_t), size_t])

# tls_config_set_ocsp_staple_mem = _fw(_lib, 'tls_config_set_ocsp_staple_mem',
                                     # c_int, [c_void_p, _gpt(uint8_t), size_t])

tls_config_set_ocsp_staple_file = _fw(_lib, 'tls_config_set_ocsp_staple_file',
                                      c_int, [c_void_p, c_char_p])

tls_config_set_protocols = _fw(_lib, 'tls_config_set_protocols', c_int,
                               [c_void_p, uint32_t])

tls_config_set_verify_depth = _fw(_lib, 'tls_config_set_verify_depth', c_int,
                                  [c_void_p, c_int])

tls_config_prefer_ciphers_client = _fw(_lib, 'tls_config_prefer_ciphers_client',
                                       None, [c_void_p])

tls_config_prefer_ciphers_server = _fw(_lib, 'tls_config_prefer_ciphers_server',
                                       None, [c_void_p])

tls_config_insecure_noverifycert = _fw(_lib, 'tls_config_insecure_noverifycert',
                                       None, [c_void_p])

tls_config_insecure_noverifyname = _fw(_lib, 'tls_config_insecure_noverifyname',
                                       None, [c_void_p])

tls_config_insecure_noverifytime = _fw(_lib, 'tls_config_insecure_noverifytime',
                                       None, [c_void_p])

tls_config_verify = _fw(_lib, 'tls_config_verify', None, [c_void_p])

tls_config_ocsp_require_stapling = _fw(_lib, 'tls_config_ocsp_require_stapling',
                                       None, [c_void_p])

tls_config_verify_client = _fw(_lib, 'tls_config_verify_client', None,
                               [c_void_p])

tls_config_verify_client_optional = _fw(_lib, 'tls_config_verify_client_optional',
                                        None, [c_void_p])

tls_config_clear_keys = _fw(_lib, 'tls_config_clear_keys', None, [c_void_p])

tls_config_parse_protocols = _fw(_lib, 'tls_config_parse_protocols', c_int,
                                 [_gpt(uint32_t), c_char_p])

tls_config_set_session_id = _fw(_lib, 'tls_config_set_session_id', c_int,
                                [c_void_p, _gpt(c_ubyte), size_t])

tls_config_set_session_lifetime = _fw(_lib, 'tls_config_set_session_lifetime',
                                      c_int, [c_void_p, c_int])

# tls_config_add_ticket_key = _fw(_lib, 'tls_config_add_ticket_key', c_int,
                                # [c_void_p, uint32_t, _gpt(c_ubyte), size_t])

tls_client = _fw(_lib, 'tls_client', c_void_p, [])

tls_server = _fw(_lib, 'tls_server', c_void_p, [])

tls_configure = _fw(_lib, 'tls_configure', c_int, [c_void_p, c_void_p])

tls_reset = _fw(_lib, 'tls_reset', None, [c_void_p])

tls_free = _fw(_lib, 'tls_free', None, [c_void_p])

# tls_accept_fds = _fw(_lib, 'tls_accept_fds', c_int, [c_void_p, _gpt(c_void_p),
                     # c_int, c_int])

tls_accept_socket = _fw(_lib, 'tls_accept_socket', c_int,
                        [c_void_p, _gpt(c_void_p), c_int])

# tls_accept_cbs = _fw(_lib, 'tls_accept_cbs', c_int, [c_void_p, _gpt(c_void_p),
                     # tls_read_cb, tls_write_cb, c_void_p])

tls_connect = _fw(_lib, 'tls_connect', c_int, [c_void_p, c_char_p, c_char_p])

# tls_connect_fds = _fw(_lib, 'tls_connect_fds', c_int,
                      # [c_void_p, c_int, c_int, c_char_p])

tls_connect_servername = _fw(_lib, 'tls_connect_servername', c_int,
                             [c_void_p, c_char_p, c_char_p, c_char_p])

tls_connect_socket = _fw(_lib, 'tls_connect_socket', c_int,
                         [c_void_p, c_int, c_char_p])

# tls_connect_cbs = _fw(_lib, 'tls_connect_cbs', c_int,
                      # [c_void_p, tls_read_cb, tls_write_cb, c_void_p, c_char_p])

tls_handshake = _fw(_lib, 'tls_handshake', c_int, [c_void_p])

tls_read = _fw(_lib, 'tls_read', ssize_t, [c_void_p, c_void_p, size_t])

tls_write = _fw(_lib, 'tls_write', ssize_t, [c_void_p, c_void_p, size_t])

tls_close = _fw(_lib, 'tls_close', c_int, [c_void_p])

tls_peer_cert_provided = _fw(_lib, 'tls_peer_cert_provided', c_int, [c_void_p])

tls_peer_cert_contains_name = _fw(_lib, 'tls_peer_cert_contains_name', c_int,
                                  [c_void_p, c_char_p])

tls_peer_cert_hash = _fw(_lib, 'tls_peer_cert_hash', c_char_p, [c_void_p])

tls_peer_cert_issuer = _fw(_lib, 'tls_peer_cert_issuer', c_char_p, [c_void_p])

tls_peer_cert_subject = _fw(_lib, 'tls_peer_cert_subject', c_char_p, [c_void_p])

tls_peer_cert_notbefore = _fw(_lib, 'tls_peer_cert_notbefore', time_t,
                              [c_void_p])

tls_peer_cert_notafter = _fw(_lib, 'tls_peer_cert_notafter', time_t, [c_void_p])

tls_conn_alpn_selected = _fw(_lib, 'tls_conn_alpn_selected', c_char_p,
                             [c_void_p])

tls_conn_cipher = _fw(_lib, 'tls_conn_cipher', c_char_p, [c_void_p])

tls_conn_servername = _fw(_lib, 'tls_conn_servername', c_char_p, [c_void_p])

tls_conn_version = _fw(_lib, 'tls_conn_version', c_char_p, [c_void_p])

# tls_load_file = _fw(_lib, 'tls_load_file', _gpt(uint8_t),
                    # [c_char_p, _gpt(size_t), c_char_p])

tls_ocsp_process_response = _fw(_lib, 'tls_ocsp_process_response', c_int,
                                [c_void_p, _gpt(c_ubyte), size_t])

tls_peer_ocsp_cert_status = _fw(_lib, 'tls_peer_ocsp_cert_status', c_int,
                                [c_void_p])

tls_peer_ocsp_crl_reason = _fw(_lib, 'tls_peer_ocsp_crl_reason', c_int,
                               [c_void_p])

tls_peer_ocsp_next_update = _fw(_lib, 'tls_peer_ocsp_next_update', time_t,
                                [c_void_p])

tls_peer_ocsp_response_status = _fw(_lib, 'tls_peer_ocsp_response_status',
                                    c_int, [c_void_p])

tls_peer_ocsp_result = _fw(_lib, 'tls_peer_ocsp_result', c_char_p, [c_void_p])

tls_peer_ocsp_revocation_time = _fw(_lib, 'tls_peer_ocsp_revocation_time',
                                    time_t, [c_void_p])

tls_peer_ocsp_this_update = _fw(_lib, 'tls_peer_ocsp_this_update', time_t,
                                [c_void_p])

tls_peer_ocsp_url = _fw(_lib, 'tls_peer_ocsp_url', c_char_p, [c_void_p])
